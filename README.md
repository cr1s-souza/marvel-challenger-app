# README #

.

### What is this repository for? ###

* Marvel Challenger
* Desafio proposto de consumir a api da marvel (https://developer.marvel.com/)

### How do I get set up? ###

* Necessário ter previamente instalado o Node (https://nodejs.org/pt-br/download/)
* Rodar o git clone disponível pelo repositório
* > npm install -g @angular/cli
* > npm install
* > ng serve
* abrir http://localhost:4200/
