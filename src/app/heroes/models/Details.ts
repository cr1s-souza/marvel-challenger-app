export interface Details {
    data: Data;
}

interface Data {
    results: DadosHeroi[];
}

interface DadosHeroi {
    id: number;
    name: String;
    thumbnail: Thumbnail;
}

interface Thumbnail {
    extension: String;
    path: String;
}