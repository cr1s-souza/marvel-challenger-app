import { Component, OnInit } from '@angular/core';
import { HeroService } from './services/hero.service';
import { Hero } from './models/Hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  // heroes: Hero;
  heroes: any;

  constructor(
    private heroService: HeroService
  ) { }

  ngOnInit(): void {
    this.getHeroes();
  }
  
  getHeroes() {
    this.heroService.getListHeroes()
    .subscribe((listheroes: Hero) => { 
        this.heroes = listheroes.data.results;
      }, (error: any) => {}
    )
  }

}
