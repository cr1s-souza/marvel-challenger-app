import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Hero } from '../models/Hero';
import { Details } from '../models/Details';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private http: HttpClient) { }

  targets = {
    getHeroes: 'characters',
    limit: '15',
  };

  getListHeroes(): Observable<Hero> {
    const url = `${environment.urlApi}/${this.targets.getHeroes}`;

    const params = new HttpParams()
    .set('limit', this.targets.limit)
    .set('ts', `${environment.tsApi}`)
    .set('apikey', `${environment.apiKey}`)
    .set('hash', `${environment.hashApi}`);
    
    return this.http.get<Hero>(url, { params });
  }

  getDetailsHero(idHero): Observable<Details> {
    const url = `${environment.urlApi}/${this.targets.getHeroes}/${ idHero }`;

    const params = new HttpParams()
    .set('limit', this.targets.limit)
    .set('ts', `${environment.tsApi}`)
    .set('apikey', `${environment.apiKey}`)
    .set('hash', `${environment.hashApi}`);
    
    return this.http.get<Details>(url, { params });
  }
}
