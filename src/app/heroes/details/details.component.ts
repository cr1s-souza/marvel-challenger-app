import { Component, OnInit } from '@angular/core';
import { HeroService } from '../services/hero.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  // heroDetails: Details;
  heroDetails: any;
  idHero

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.idHero = this.route.snapshot.params.idHero;
    this.getDetailsHero(this.idHero)
            
  }

  getDetailsHero(idHero) {
    this.heroService.getDetailsHero(idHero)
    .subscribe((details: any) => { 
        this.heroDetails = details.data.results[0];

      }, (error: any) => {}
    )
  }

}
